#!/usr/bin/env python
import requests
import json
import sys
from xml.etree import ElementTree
import urllib
import codecs
import re
import datetime
try:
    from urllib.parse import urlparse
except ImportError:
     from urlparse import urlparse
from osgeo import ogr
from osgeo import osr


# PARAMETERS & SETTIGNS

# the CAP-feeds to include 
# urls = ['https://alerts.fmi.fi/cap/feed/atom_fi-FI.xml','https://vesi.fi/karttapalvelu/dev/varoitusCap5.xml']
# urls = ['https://www.vesi.fi/karttapalvelu/dev/atom_fi-FI.xml','https://vesi.fi/karttapalvelu/dev/varoitusCap5.xml']
urls = ['https://wwwi2.ymparisto.fi/i2/CAP/SYKE_CAP_current.atom','https://alerts.fmi.fi/cap/feed/atom_fi-FI.xml']
# urls = ['https://www.vesi.fi/karttapalvelu-dev/atom_fi-FI_small2.xml']
# default output file name / path
defaultFileName = 'alerts.json'
# the event types to include
eventCodesToInclude = ['12;flooding','seaWaterHeight','rain']
# how old alerts to include (expires-field of the alert)
pastDaysToInclude = 10
# SYKE proxy servers, if needed
useProxies = True
proxies = {
    'http': 'http://gate102.vyh.fi:81',
    'https': 'http://gate102.vyh.fi:81'
}


def parse_CAPs_to_geoJSON(urls, filename):
    '''
    Retrieves the CAP XMLs from listed URLs
    and parses a single geojson file combining 
    the CAPS data. 
    
    Parameters: 
      - filename: the name/path of the resulting geojson
    '''
    
    #region spatial layer and geojson file

    sourceCS = osr.SpatialReference()
    sourceCS.ImportFromEPSG(4326)
    targetCS = osr.SpatialReference()
    targetCS.ImportFromEPSG(3067)
    transform = osr.CoordinateTransformation(sourceCS, targetCS)
    driver = ogr.GetDriverByName('GeoJSON')
    data_source = driver.CreateDataSource(filename)
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(3067)
    layer = data_source.CreateLayer("CAP-alerts", srs, ogr.wkbPolygon, options=['COORDINATE_PRECISION=0'])

    #endregion

    #region attributes of the new goejson

    field_def = ogr.FieldDefn("identifier", ogr.OFTString)
    field_def.SetWidth(200)
    layer.CreateField(field_def)
    field_def = ogr.FieldDefn("event_code", ogr.OFTString)
    field_def.SetWidth(50)
    layer.CreateField(field_def)
    field_def = ogr.FieldDefn("severity", ogr.OFTString)
    field_def.SetWidth(50)
    layer.CreateField(field_def)
    field_def = ogr.FieldDefn("severityIndex", ogr.OFTInteger)
    layer.CreateField(field_def)
    field_def = ogr.FieldDefn("onset", ogr.OFTString)
    field_def.SetWidth(50)
    layer.CreateField(field_def)
    field_def = ogr.FieldDefn("expires", ogr.OFTString)
    field_def.SetWidth(50)
    layer.CreateField(field_def)
    field_def = ogr.FieldDefn("geomType", ogr.OFTString)
    field_def.SetWidth(50)
    layer.CreateField(field_def)

    field_def = ogr.FieldDefn("type_fi", ogr.OFTString)
    field_def.SetWidth(50)
    layer.CreateField(field_def)
    field_def = ogr.FieldDefn("type_sv", ogr.OFTString)
    field_def.SetWidth(50)
    layer.CreateField(field_def)
    field_def = ogr.FieldDefn("type_en", ogr.OFTString)
    field_def.SetWidth(50)
    layer.CreateField(field_def)
    field_def = ogr.FieldDefn("area_fi", ogr.OFTString)
    field_def.SetWidth(160)
    layer.CreateField(field_def)
    field_def = ogr.FieldDefn("area_sv", ogr.OFTString)
    field_def.SetWidth(160)
    layer.CreateField(field_def)
    field_def = ogr.FieldDefn("area_en", ogr.OFTString)
    field_def.SetWidth(160)
    layer.CreateField(field_def)
    field_def = ogr.FieldDefn("desc_fi", ogr.OFTString)
    field_def.SetWidth(300)
    layer.CreateField(field_def)
    field_def = ogr.FieldDefn("desc_sv", ogr.OFTString)
    field_def.SetWidth(300)
    layer.CreateField(field_def)
    field_def = ogr.FieldDefn("desc_en", ogr.OFTString)
    field_def.SetWidth(300)
    layer.CreateField(field_def)

    #endregion

    #requests session
    s=requests.Session()
    if useProxies:
        s.proxies = proxies

    for url in urls:
        print('Retrieving CAP from %s' % (url))
        r = s.get(url, verify=False)
        if(r.status_code!=200):
            print('request failed for url %s, status code %s' % (url, str(r.status_code)))
            break
        r.encoding='utf-8'
        count = 0
        #tree = ElementTree.fromstring(r.content.decode("utf-8"))
        #tree = ElementTree.fromstring(codecs.decode(r.content, "utf-8"))
        tree = ElementTree.fromstring(r.content)
        #print(r.content)
        roottag = tree.tag
        # print(roottag)

        rootnamespace = ''
        if roottag[0] == '{':
            rootnamespace = '{' + roottag[1:].split('}')[0] + '}'
        cap_ns = '{urn:oasis:names:tc:emergency:cap:1.2}'

        # CAP format has updates and cancels, collect these first
        skipTheseIds = []
        for entry in tree.iter(rootnamespace + 'entry'):
            content = entry.find(rootnamespace + 'content')
            alert = content.find(cap_ns + 'alert')
            # filter alerts
            if alert.find(cap_ns + 'status').text.lower()=='actual' and (alert.find(cap_ns + 'msgType').text.lower()=='cancel' or alert.find(cap_ns + 'msgType').text.lower()=='update'):
                try:  # fails, if there are no references (and for n other reasons...)
                    references = alert.find(cap_ns + 'references').text.split(' ')
                    for reference in references:
                        try:
                            updatedId = reference.split(',')[1]
                            if updatedId not in skipTheseIds:
                                skipTheseIds.append(updatedId)
                        except:
                            pass
                except:
                    pass
        
        # loop alerts
        for entry in tree.iter(rootnamespace + 'entry'):
            content = entry.find(rootnamespace + 'content')
            alert = content.find(cap_ns + 'alert')
            print(' Alert ' + alert.find(cap_ns + 'identifier').text) 
            # filter away tests and outdated alerts
            if alert.find(cap_ns + 'status').text.lower()!='actual' or alert.find(cap_ns + 'identifier').text in skipTheseIds:
                print('   -> skipping as non-actual or later updated') 
                continue
            # include only alerts and updates
            if alert.find(cap_ns + 'msgType').text.lower()!='alert' and alert.find(cap_ns + 'msgType').text.lower()!='update':
                print('   -> skipping due to wrong msgType') 
                continue
            # get area polyongs from first info-element
            info = alert.find(cap_ns + 'info')
            # filter based on expiration
            expiresDT = datetime.datetime.strptime((info.find(cap_ns + 'expires').text)[:10], "%Y-%m-%d")
            timelimit = datetime.datetime.now() - datetime.timedelta(days=pastDaysToInclude)
            if (expiresDT < timelimit):
                print('   -> skipping as outdated') 
                continue
            # finally, filter based on event type
            eventCode = info.find(cap_ns + 'eventCode')
            eventVentCodeValue = eventCode.find(cap_ns + 'value').text
            if eventVentCodeValue in eventCodesToInclude:
                print('   Event: ' + info.find(cap_ns + 'event').text + ' ' + info.find(cap_ns + 'onset').text) 
                for areaIndex, area in enumerate(info.iter(cap_ns + 'area')):   
                    print('      area ' + str(areaIndex) + ': ' + area.find(cap_ns + 'areaDesc').text)
                    # create feature and its geometry
                    feature = ogr.Feature(layer.GetLayerDefn())
                    poly = ogr.Geometry(ogr.wkbPolygon)  
                    for polygon in area.iter(cap_ns + 'polygon'):
                        ring = ogr.Geometry(ogr.wkbLinearRing)
                        # polygonArr = polygon.text.split(' ')
                        polygonArr = re.split('\s',polygon.text)
                        for pair in polygonArr:
                            try:
                                if pair!='':
                                    coordinates = pair.split(',')
                                    ring.AddPoint(float(coordinates[1]),float(coordinates[0]))
                            except:
                                pass

                        ring.Transform(transform)
                        poly.AddGeometry(ring)
                    simplePoly = poly.SimplifyPreserveTopology(1000)      
                    feature.SetGeometry(simplePoly.Clone())
                    # fill in the attributes
                    feature.SetField("identifier", alert.find(cap_ns + 'identifier').text)
                    feature.SetField("event_code", eventVentCodeValue)
                    strSeverity = info.find(cap_ns + 'severity').text
                    if strSeverity.lower()=="minor":
                        # feature.SetField("severityIndex", 1)
                        continue
                    elif strSeverity.lower()=="moderate":
                        feature.SetField("severityIndex", 2)
                    elif strSeverity.lower()=="severe":
                        feature.SetField("severityIndex", 3)
                    elif strSeverity.lower()=="extreme":
                        feature.SetField("severityIndex", 4)
                    else:
                        feature.SetField("severityIndex", 0)
                    # special case: low water alert
                    if eventVentCodeValue.lower()=='seawaterheight':
                        for parameter in info.iter(cap_ns + 'parameter'):                            
                            if parameter.find(cap_ns + 'valueName').text.lower()=='eventcause' and parameter.find(cap_ns + 'value').text.lower()=='shallowwater':
                                strSeverity = 'low'
                                break
                    feature.SetField("severity", strSeverity)
                    feature.SetField("onset", info.find(cap_ns + 'onset').text)
                    feature.SetField("expires", info.find(cap_ns + 'expires').text)

                    feature.SetField("geomType", 'polygon')
                    # loop languages
                    for langInfo in alert.iter(cap_ns + 'info'):
                        try:
                            langIndicator = langInfo.find(cap_ns + 'language').text[:2]
                            feature.SetField(("type_"+langIndicator), langInfo.find(cap_ns + 'event').text)
                            feature.SetField(("desc_"+langIndicator), langInfo.find(cap_ns + 'description').text)
                            areas = list(langInfo.iter(cap_ns + 'area'))
                            if areas[areaIndex] is not None:
                                feature.SetField(("area_"+langIndicator), areas[areaIndex].find(cap_ns + 'areaDesc').text)
                            else:
                                feature.SetField(("area_"+langIndicator), 'unknown area')
                        except:
                            break
                    # store
                    layer.CreateFeature(feature)
                    count = count + 1
                    feature = None
            else:
                print('   -> skipping due to event code') 
        print('  created ' + str(count) + ' features')

    # Save and close the data source
    data_source = None
    print('Alert-GeoJSON written in ' + filename)

    #except Exception as e:
        #print('[get_IL_CAP] PArsing IL CAP for %s failed with exception: %s' % (url, str(e)))
    return {}





# *** MAIN ***


if len(sys.argv) > 1:
    filename = sys.argv[1]
else:
    filename = defaultFileName

parse_CAPs_to_geoJSON(urls, filename)

