#!/usr/bin/env python
import requests
import json
import sys
from xml.etree import ElementTree
import urllib
import codecs
try:
    from urllib.parse import urlparse
except ImportError:
     from urlparse import urlparse
from osgeo import ogr
from osgeo import osr




def get_IL_CAP(url, filename):
    '''
    Retrieves the CAP XML and parses an
    array of the desired events.
    '''
    
    sourceCS = osr.SpatialReference()
    sourceCS.ImportFromEPSG(4326)
    targetCS = osr.SpatialReference()
    targetCS.ImportFromEPSG(3067)
    transform = osr.CoordinateTransformation(sourceCS, targetCS)
    driver = ogr.GetDriverByName('GeoJSON')
    data_source = driver.CreateDataSource(filename)
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(3067)
    layer = data_source.CreateLayer("alerts", srs, ogr.wkbPolygon, options=['COORDINATE_PRECISION=0'])
    field_name = ogr.FieldDefn("Title", ogr.OFTString)
    field_name.SetWidth(50)
    layer.CreateField(field_name)

    alertArray = []
    proxies = {
        'http': 'http://gate102.vyh.fi:81',
        'https': 'http://gate102.vyh.fi:81'
    }
    s=requests.Session()
    s.proxies = proxies
    r = s.get(url, verify=False)
    if(r.status_code!=200):
        print('get_IL_CAP failed for %s, status code %s' % (url, str(r.status_code)))
        return {}
    #tree = ElementTree.fromstring(r.content.decode("utf-8"))
    #tree = ElementTree.fromstring(codecs.decode(r.content, "utf-8"))
    tree = ElementTree.fromstring(r.content)
    #print(r.content)
    roottag = tree.tag
    # print(roottag)

    rootnamespace = ''
    if roottag[0] == '{':
        rootnamespace = '{' + roottag[1:].split('}')[0] + '}'
    cap_ns = '{urn:oasis:names:tc:emergency:cap:1.2}'

    index = 0
    for entry in tree.iter(rootnamespace + 'entry'):
        # print(entry.find(rootnamespace + 'title').text + ':')
        content = entry.find(rootnamespace + 'content')
        alert = content.find(cap_ns + 'alert')
        # print('status: ' + alert.find(cap_ns + 'status').text)
        info = alert.find(cap_ns + 'info')
        language = info.find(cap_ns + 'language')
        eventCode = info.find(cap_ns + 'eventCode')
        print('event code ' + eventCode.find(cap_ns + 'value').text + ', lang ' + language.text)
        if eventCode.find(cap_ns + 'value').text=='seaWind' and language.text=='fi-FI':
            # print(' parsing...')
            # for area in info.iter(cap_ns + 'area'):  
            for areaIndex, area in enumerate(info.iter(cap_ns + 'area')):           
                print('   area ' + str(areaIndex) + ': ' + area.find(cap_ns + 'areaDesc').text)
                feature = ogr.Feature(layer.GetLayerDefn())
                feature.SetField("Title", eventCode.find(cap_ns + 'value').text)
                poly = ogr.Geometry(ogr.wkbPolygon)  
                for polygon in area.iter(cap_ns + 'polygon'):
                    ring = ogr.Geometry(ogr.wkbLinearRing)
                    polygonArr = polygon.text.split(' ')
                    for pair in polygonArr:
                        coordinates = pair.split(',')
                        ring.AddPoint(float(coordinates[1]),float(coordinates[0]))
                        # point = ogr.Geometry(ogr.wkbPoint)
                        # point.AddPoint(float(coordinates[1]),float(coordinates[0]))
                        # point.Transform(transform)

                    ring.Transform(transform)
                    poly.AddGeometry(ring)
                simplePoly = poly.SimplifyPreserveTopology(1000)
                #poly.SimplifyPreserveTopology(10000)            
                feature.SetGeometry(simplePoly.Clone())
                layer.CreateFeature(feature)
                feature = None

    # Save and close the data source
    data_source = None
    print('Alert-GeoJSON written in ' + filename)

    #except Exception as e:
        #print('[get_IL_CAP] PArsing IL CAP for %s failed with exception: %s' % (url, str(e)))
    return {}





# *** MAIN ***

url = 'https://alerts.fmi.fi/cap/feed/atom_fi-FI.xml'
#url = 'https://www.vesi.fi/karttapalvelu/dev/atom_fi-FI.xml'

if len(sys.argv) > 1:
    filename = sys.argv[1]
else:
    filename = 'alerts.json'

get_IL_CAP(url, filename)

